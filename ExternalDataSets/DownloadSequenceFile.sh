#!/bin/bash

# Download gene annotations from from igbquickload.org
wget http://igbquickload.org/A_thaliana_Jun_2009/TAIR10.bed.gz

# Download the .2bit genome file 
wget http://igbquickload.org/A_thaliana_Jun_2009/A_thaliana_Jun_2009.2bit

# generate fasta file from 2bit file
twoBitToFa A_thaliana_Jun_2009.2bit A_thaliana_Jun_2009.fa

# change chromosome names in fasta file from Chr1 to chr1, etc, to match bed file.
sed 's/>Chr/>chr/' A_thaliana_Jun_2009.fa > temp.fa
mv temp.fa A_thaliana_Jun_2009.fa