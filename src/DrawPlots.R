# Common Plots
source("../src/ProjectWide.R")
suppressPackageStartupMessages(library(gplots))

makeClusterPlot=function(hc,lab=hc$labels,lab.col=rep(1,length(hc$labels)),
                         hang=0.1,axes=F, ...) {
  # hc - an object of class hclust (the output of the hclust function)
  y = rep(hc$height,2)
  x = as.numeric(hc$merge)
  y = y[which(x<0)]
  x = x[which(x<0)]; x=abs(x)
  y = y[order(x)]; x = x[order(x)]
  plot(hc,labels=FALSE,hang=hang,axes=axes, ...)
  text(x=x, y=y[hc$order]-(max(hc$height)*hang),
       labels=lab[hc$order], col=lab.col[hc$order],
       srt=90, adj=c(1,0.5),xpd=NA, ... )
}



# Plot a matrix like a heat map. Ideal for the $r output of rcorr(){Hmisc}
# see also image()
plotChart=function(chart, chartText=NULL, range=1, width=3, showText=T, 
                   main="title", cex.text=1, mar=c(1.2,4,1,0)){
  # chart - a matrix of values to be printed in a heat map style
  # chartText - an additional matrix with the same dimensions as chart 
  #             from which to pull the text while still drawing the colors from chart.
  # range - the greatest absolute value in the range of values
  # width - factor for how wide the plot should be
  # showText - T/F should the values be printed in the squares
  # cex.text - text size factor applied to the values in the chart.
  par(mar=mar, oma=c(0,0,0,0))
  # make a series of colors
  my_palette <- c(colorRampPalette(c("blue", "lightgreen"))(n = 100),
                  "#FFFFFF", #white for zero
                  colorRampPalette(c("orange", "red"))(n = 100))
  # name the colors by numbers
  refVals=seq(-range, range, length.out=length(my_palette))
  names(my_palette)=refVals
  #function to get values for colors
  getCol=function(x){
    n=max(refVals[refVals<=x])
    return(my_palette[as.character(n)])
  }
  # assign colors to values in chart
  cols=sapply(t(chart), getCol)
  # plot squares of color for each value
  x=rep(1:ncol(chart), nrow(chart))
  y=rep(nrow(chart):1, each=ncol(chart))
  plot(x=1, y=1, xlim=c(0,max(x)+1), ylim=c(0,max(y)+1),
       type="n", xaxt="n", yaxt="n", ylab="", fg="white", asp=1/width)
  symbols(x=x, y=y, add=T, fg=gray(.95), inches=F, bg=cols, 
          rectangles = matrix(data=1, byrow=T, nrow=length(x), ncol=2))
  # Labels
  # main
  text(y=nrow(chart)+.5, x=ncol(chart)/2, labels=main, xpd=t, cex=2, pos=3)
  # x-axis
  text(x=1:ncol(chart), y=0, labels=colnames(chart), adj=1, srt=45, xpd=T)
  # y-axis
  text(y=nrow(chart):1, x=0.4, labels=rownames(chart), adj=1, xpd=T)
  # add values as text
  if (showText) {
    if (is.null(chartText)){chartText = chart}
    text(x=x, y=y, labels=t(chartText), cex=cex.text)}
}




# Draw a bar plot representing individual sample values for a given gene.
### This function assumes that the getSampleColors and getRPKM functions are available and 
### that it will return a data frame with only columns for the requested samples and 
### that the row names will be gene names.
DrawGeneSampleValues = function(AGI="AT4G37750", 
                                samples=getSampleNames(), 
                                main=AGI,
                                preReadFpkm=NULL){
  # AGI - an AGI code for a gene of interest, should mate a row name in getRPKM()
  # samples - the samples to include in plot
  # main - a main title for the plot
  par(las=1, mar=c(3,3,1,1), mgp=c(1.5,0.5,0))
  samples=samples[length(samples):1]
  if (is.null(preReadFpkm)){
    fpkm=getRPKM(samples, ave=F, sd=F) #from ProjectVariables.R, assumes gene names are row names.
  }else{
    fpkm=getRPKM(samples, ave=F, sd=F, preReadD=preReadFpkm) #from ProjectVariables.R, assumes gene names are row names. 
  }
  fpkm=as.matrix(fpkm[AGI,samples])
  bp=barplot(as.numeric(fpkm), col=getSampleColors(samples), 
             horiz=T, xlim=c(0,(max(fpkm)*1.05)),
             names.arg=colnames(fpkm), las=1,
             main=main, xlab="RPKM")
  box()
}



# Draw a bar plot representing mean group values for a given gene.
### This function assumes that the getGroupColors and getRPKM functions are available and 
### that it will return a data frame with only columns for the requested samples and 
### that the row names will be gene names, and 
### that the RPKM table will have columns with .ave and .sd
DrawGeneGroupValues = function(AGI="AT4G37750", 
                               groups=NULL, 
                               main=AGI, 
                               preReadFpkm=NULL){
  if(is.null(groups)){
    groups = getSampleGroups()
    groups = unique(groups[grep("2|4|8",groups)])
  }
  par(las=1, mar=c(3,3,1,1), mgp=c(1.5,0.5,0))
  if (is.null(preReadFpkm)){
    fpkm=getRPKM(groups, ave=T, sd=T)
  }else{
    fpkm=getRPKM(groups, ave=T, sd=T, preReadD=preReadFpkm)
  }
  #fpkm=fpkm[,grep("ave|sd", names(fpkm))]
  fpkm=fpkm[AGI,grep("ave|sd", names(fpkm))]
  aves=as.matrix(fpkm[,grep("ave", names(fpkm))])
  colnames(aves) = gsub(".ave", "", colnames(aves))
  sds=as.matrix(fpkm[,grep("sd", names(fpkm))])
  colnames(sds) = gsub(".sd", "", colnames(sds))
  bp=barplot(as.numeric(aves), col=getSampleGroupColors(groups), 
             ylim=c(0,(max(aves+sds)*1.05)),
             names.arg=colnames(aves), las=1,
             main=main, ylab="RPKM")
  bp=as.vector(bp)
  names(bp)=colnames(aves)
  # standard deviation bars
  for (i in groups) {
    x=bp[i]
    y=aves[,i]
    se=sds[,i]
    width=.1
    # vertical line
    segments(x0=x, y0=y-se, y1=y+se)
    # The lower bar
    segments(x0=x-width, y0=y-se, x1=x+width)
    # The upper bar
    segments(x0=x-width, y0=y+se, x1=x+width)
  }
  box()
}


DrawGeneExprLines <- function(AGI="AT4G37750", 
                              groups=NULL, samples=getSampleNames(),
                              main=AGI,
                              preReadFpkm=NULL,
                              addLines=T, addPoints=T, addArrows=F,
															addToPlot=FALSE,
															whenDE=NULL, FDR=NULL){
  if(is.null(groups)){
    groups = getSampleGroups()
    groups = unique(groups[grep("2|4|8",groups)])
  }
  if (is.null(preReadFpkm)){
    fpkm=getRPKM(c(groups, samples), ave=T, sd=T)
  }else{
    fpkm=getRPKM(c(groups, samples), ave=T, sd=T, preReadD=preReadFpkm)
  }
  fpkm.group=fpkm[AGI,grep("ave|sd", names(fpkm))]
  aves=as.matrix(fpkm.group[,grep("ave", names(fpkm.group))])
  colnames(aves) = gsub(".ave", "", colnames(aves))
  sds=as.matrix(fpkm.group[,grep("sd", names(fpkm.group))])
  colnames(sds) = gsub(".sd", "", colnames(sds))
  
  # group level plotting stuff
  xg.time = as.numeric(substr(groups, 2,2))
  cat.treat = substr(groups, 1,1)
  group.c = which(cat.treat=="C")
  group.t = which(cat.treat=="T")
  col.c = getSampleGroupColors("C4")
  col.t = getSampleGroupColors("T4")
    
  # get sample values to add points
   #from ProjectVariables.R, assumes gene names are row names.
  if (is.null(preReadFpkm)){
    fpkm=getRPKM(samples, ave=F, sd=F)
  }else{
    fpkm=getRPKM(samples, ave=F, sd=F, preReadD=preReadFpkm) 
  }
  fpkm.s=as.matrix(fpkm[AGI,samples])
  x.true = as.numeric(substr(samples, 2,2))
  x.time=jitter(x.true, .2)
  names(x.time) = samples #helpful in drawing arrows
  col.s = getSampleColors(samples)
  
  if (!addToPlot){
  	# make empty plot
  	par(las=1, mar=c(3,3,3,1), mgp=c(1.5,0.5,0))
  	plot(x=x.time, y=fpkm.s, type="n", ylab="FPKM", xlab="time after treatment (hours)", xaxt="n")
  	title(main)
  	axis(side=1, at=x.true)
  }
  
  if (addLines){
    # add lines
    points(x=xg.time[group.c], y=aves[group.c], type="l", col=col.c, lwd=3)
    points(x=xg.time[group.t], y=aves[group.t], type="l", col=col.t, lwd=3)
  }
  
  if (addPoints){
    # add individual points
    points(x=x.time, y=fpkm.s, col=col.s, pch=16)
  }
  
  if (addArrows){
    # add arrows to indicate samples from the same flatpair
    # which samples do you have a treament and a control for?
    set = intersect(gsub('C',"",samples), 
                    gsub('T',"",samples))
    setC = paste0('C', set)
    setT = paste0('T', set)
    arrows(x0=x.time[setC],
           x1=x.time[setT],
           y0=fpkm.s[,setC],
           y1=fpkm.s[,setT], 
           col="brown", lwd=2, code=2, length=.2, angle=20)
  }
  
  if (!is.null(whenDE) & !is.null(FDR)){
  	# add red * to indicate which time points are DE,
  	# and print the FDR used to define DE
  	yStars = par("usr")[3]
  	points(x=whenDE, y=rep(yStars, length(whenDE)), pch="*", col="red", cex=2, xpd=T)
  	text(labels=paste0("FDR=", FDR), x=6, y=yStars, pos=1, col="red", xpd=T)
  }
  
}



# draw a venn diagram of the genes that are DE in each time point.
drawVennDEgenes <- function(DEresults = getDEtable(),
														comparison=NULL,
														maxFDR=NULL){
	# DEresults - data frame of differential expression results, the output of getDEtable().
	#           This dataframe should include columns with FDR values for each 
	#           gene with columns names: fdrCol = paste0("fdr.", comparison) for each comparison,
	#           and ids stored in iether row names or a "gene" column.
	# comparison - the comparisons to include (by default - the comparisons are taken from the table - up to 4
	# maxFDR - named vector giving the FDR to use in interpreting the results for each time point.
	#        if not given, the project-wide FDR is used for all comparisons.
	#
	# The venn function is taken from ggplot
	# library(gplots)
	#
	# get values for NULL arguments
	if (is.null(row.names(DEresults))){
		row.names(DEresults) = DEresults$gene
		if (is.null(row.names(DEresults))){
			stop("DEresults must have identifiers as row names, or in a 'gene' column.")
		}
	}
	if (is.null(comparison)){
		colNames = grep("^fdr.", names(DEresults), value = T)
		comparison = gsub("^fdr.", "", colNames)
	}
	if (is.null(maxFDR)){
		projectWideFDR = getFDR()
		maxFDR = rep(projectWideFDR, length(comparison))
		names(maxFDR) = comparison
		print("FDR cuttoff for each comparison:")
		print(maxFDR)
	}
	#
	# Pick out DE genes for each comparison
	DElist = as.list(comparison)
	names(DElist) = comparison
	for (comp in comparison){
		fdrCol = paste0("fdr.", comp)
		w = which(DEresults[fdrCol] <= maxFDR[comp])
		DElist[[comp]] = row.names(DEresults)[w]
	}
	DElist = lapply(DElist, as.character)
	#
	# The plot function only supports diagrams for up to 5 groups
	if (length(DElist) > 5){
		warning("The plot function only supports diagrams for up to 5 groups")
		DElist = DElist[1:5]
		}
	#
	# draw plot
	par(oma=c(0,0,0,0), mar=c(0,0,0,0))
	ven=venn(DElist)
	# Since the call to create ven was the last call, 
	# ven can be captured if the results of the function are saved to a variable.
	#	The returned value is typically not desired, so skipping the return statement means
	# that the user does not need to redirect the function output every time the function is used.
	#return(ven)
}


