---
title: "Make expression table and perform sanity checks on gene expression"
output:
  html_document:
    toc: true
---

#Introduction
FeatureCounts was used to determine the number of reads that overlapped each gene. The regions for each gene were given as an SAF file (see ExternalDataSets/MakeGeneRegions.Rmd) based on the TAIR 10 genome annotations. For details, see FeatureCounts output in the data subfolder.

The counts from FeatureCounts are reads per feature. We will want to make comparisons between samples, and for that we will need to have the counts normalized for library size.  To make comparisons between genes, we will need to normalize for gene size. In this mark down we will convert values to RPKM (**R**eads **P**er **K**ilobase transcript per **M**illion reads per library).

A few quality control questions will allow us to check that the method is working correctly and that the data are what we expect.

**Questions**    

 * How many reads aligned in each library, and how many were assigned to genes?
 * How are the reads distributed across genes?
 * Are possible positive control genes higher in the treated than in the non-treated samples?
 * Do the samples in the same time collection have similar values for clock genes?

# Analysis 

```{r}
source("../src/ProjectWide.R")
source("../src/DrawPlots.R")
library(knitr)
```

## Read counts per gene expression data

```{r}
counts=getRawCounts() #defined in ProjectWide.R, see external definitions below
```
```{r echo=FALSE}
options(scipen=999)
```

Make sure the columns and the samples variables are in the same order.
```{r}
sampleNames = getSampleNames()
counts=counts[,getSampleFiles(sampleNames)]
```

Get sample group names. The following code uses a file called `r "../Samples.txt", which maps sample codes onto fastq files names.

If the Samples.txt table notes that a sample should be excluded (ie, it does not have a 2, 4, or 8 in the sample group name) it's included in the counts table but not included in the RPKM file output below. 

```{r}
sample_types = getSampleGroups(sampleNames)
sample_types = sample_types[grep("2|4|8", sample_types)]
exclude=setdiff(sampleNames, names(sample_types))
```
There are `r length(exclude)` excluded samples`r ifelse(length(exclude)>0, paste(":", paste(exclude, collapse=", ")), ".")`

Convert file names to the  names used in R.
```{r}
names(sampleNames) = getSampleFiles(sampleNames)
names(counts) = sampleNames[names(counts)]
```


## Summarize read counts by library 

How many millions of reads aligned from each sample?
```{r}
fname="../Alignment/results/AlignmentSummaryTable.txt"
al = read.delim(fname, stringsAsFactors=F) #al for ALigned
al$SampleName = sampleNames[al$SampleName]
row.names(al) = al$SampleName
al=al[names(counts),"SingleMapping"]
mal = al/1e+6 # mal for Million ALigned
names(mal)=names(counts)
mal = round(mal, 1)
```

Each library produced between `r min(mal)` and `r max(mal)` million aligned reads, with an average of `r round(mean(mal),1)`.

How many millions of reads in each library were assigned to a gene?

```{r}
fname="data/ANT-GR_CountsPerGene.summary.txt"
assigned = as.numeric(read.delim(fname, row.names = 1)["Assigned",names(sampleNames)])

mass = assigned/1e+6 # mal for Million ASSigned
names(mass)=sampleNames
mass = round(mass, 1)
```

Each library had between `r min(mass)` and `r max(mass)` million assigned reads, with an average of `r round(mean(mass),1)`.

Show aligned and assigned read counts.

```{r echo=FALSE, fig.height=5, fig.width=6}
par(mfrow=c(1,2))

# Aligned
colors=getSampleColors(names(mal))
xlab="Mapped reads per library (millions)"
par(las=1)
par(mar=c(3,4,1,2))
par(mgp=c(1.5,0.5,0))
barplot(as.numeric(mal),col=colors,horiz=T,names.arg=names(mal),
        xlim=c(0,max(mal)+2),
        cex.names=1,main="",xlab=xlab)
box()
text(par("usr")[2]/2,par("usr")[4],"Aligned Reads", adj=c(.5,-.2), cex=1.2, xpd=T)

# Assigned
xlab="Counted reads per library (millions)"
barplot(as.numeric(mass),col=colors,horiz=T,names.arg=names(mal),
        xlim=c(0,max(mass)+2),
        cex.names=1,main="",xlab=xlab)
box()
text(par("usr")[2]/2,par("usr")[4],"Assigned Reads", adj=c(.5,-.2), cex=1.2, xpd=T)
```

Either aligned or assigne read counts could be used to represent library size in the normalization. The results would be very similar whichever was used. 

## Obtain transcript sizes for RPKM normalization

The transcript sizes were calculated from the TAIR10 gene models in GetTranscriptSizes.Rmd.

```{r}
fname="../ExternalDataSets/results/tx_size.txt.gz"
ts = read.delim(fname, as.is=T) #ts for transcript size
kb = ts$size/1e3
names(kb) = ts$gene
```
```{r echo=FALSE, fig.width=6, fig.height=2}
par(las=1, mar=c(3,5,1,1), mgp=c(1.5,.5,0))
hist(kb, main="", xlab="gene size (kb)", 
     col="gray", ylab="", breaks=100)
text(10, 2000, "frequency of gene sizes", cex=1.5, xpd=T)
```

Transcript sizes range from `r round(min(kb),2)` kb to `r round(max(kb),0)` kb, with an average of about `r round(mean(kb),1)`.

## Calculate RPKM expression values for genes

Divide all values in each column (sample) of counts by the library size (in millions). Use total mapped reads (mal) to represent library size, this includes reads that were not assigned to any gene.

```{r}
rpm = counts
for (sample in names(rpm)){
  rpm[,sample] = counts[,sample]/mal[sample]
}
```

Divide all values in each row (gene) of rpm by the transcript size (in kilobases).    
```{r}
rpkm = rpm
for (sample in names(rpkm)){
  rpkm[,sample] = rpm[,sample]/kb[row.names(rpkm)]
}
```


After adjusting for library size and transcript size, **how evenly are reads distributed accross genes?**

Truncate the figure to 80 RPKM. Beyond that, the values are so low they are hard to see see and they extend to very high RPKM values. Do not include genes with 0 counts.

```{r echo=FALSE, fig.width=9, fig.height=9}
par(mfrow=c(6,4), las=1, mar=c(3,4,1,1), mgp=c(1.5,.5,0))

df=as.data.frame(matrix(data=NA, nrow=length(counts), ncol=4))
row.names(df)=names(counts)
names(df)=c("above.0", "above.80", "the90thPercentile", "percent")

for (sample in names(counts)){
  #make plots
  hist(rpkm[,sample][rpkm[,sample]>0 & rpkm[,sample]<80], main="", xlab="normalized counts per gene (RPKM)", 
       col=colors[sample], ylab="", xpd=T)
  text(50, 5000, sample, cex=2)
  if (substr(sample, 4, 4)==1){ # if this plot is on the far left
    mtext("number of genes", side=2, line=2.5, las=0, cex=.7) # plot y-axis label
  }
  
  #fill in df
  top=10
  df[sample,"above.0"]=sum(rpkm[,sample]>0)
  df[sample,"above.80"]=sum(rpkm[,sample]>80)
  x=rpkm[,sample][rpkm[,sample]>0]
  prob=((100-top)/100)
  highScore=round(quantile(x,prob),1)
  df[sample,"the90thPercentile"]=highScore
  
  #determine percentage of counts attributed to top genes
  topOnes=row.names(rpkm)[rpkm[,sample]>highScore]
  num=sum(rpkm[topOnes,sample])
  denom=sum(rpkm[,sample])
  percent=100*num/denom
  df[sample,"percent"]=percent
  }
```  
```{r echo=F}
kable(df, col.names=c("genes with >= 1 read", "genes with mean RPKM >80", "90th percentile value (RPKM)", "% of reads in top 10% of genes"), digits=1)
```


 * Each library had at least 1 read for about `r round(mean(df$above.0), -3)` genes. 
 * The top `r top`% of all genes in terms of counts represent `r round(mean(percent),0)`% of all normalized counts.
 * Those top `r top`% had values of ~`r round(mean(df$the90thPercentile),0)`  to ~`r round(max(rpkm),-3)`.

## Write RPKM expression values per gene to a file

## Calculate average RPKM per gene

Add a column for each group indicating the group average, and a column for the group standard deviation.
```{r}
add.averages=function(fpm){
  for (sample_type in unique(sample_types)) {
    samples=names(sample_types[sample_types==sample_type])
    #
    ave=apply(fpm[,samples],1,mean)
    fpm=cbind(fpm,ave)
    names(fpm)[ncol(fpm)]=paste0(sample_type,'.ave')
    #
    sd=apply(fpm[,samples],1,sd)
    fpm=cbind(fpm,sd)
    names(fpm)[ncol(fpm)]=paste0(sample_type,'.sd')
    }
  fpm=cbind(gene=row.names(fpm), fpm)
  return(fpm)
  }
rpmToWrite = add.averages(rpm)
rpkmToWrite = add.averages(rpkm)
```


**Write RPKM table to a file if required**

```{r}
fname = "results/RPKM.txt"
if (!file.exists(paste0(fname,'.gz'))){
  write.table(rpkmToWrite, fname,sep='\t',quote=F,row.names=F)
  system(paste("gzip -f", fname))
}
```

## Perform sanity checks on the data using RPKM expression values

### Evalute RPKM as a normalization method using a control gene (actin)
   
Actin 2 (AT3G18780) is often used as a reference gene in qRT-PCR and as a loading control in RNA gels. Its expression is stable across samples types, with low variance within samples belonging to the same group. 

**What was expression of actin before and after normalizing by library size?**

```{r echo=FALSE, fig.width=5, fig.height=5}
par(mfrow=c(2,1), las=1, mar=c(1.5,4.1,4.1,2))
bp=barplot(t(counts["AT3G18780",]), main="actin - before", names.arg="",
        col=colors, xlab="", ylab="reads", beside=T)
n=seq(1,length(sampleNames),4)+2
text(x=bp[n], y=0, labels=sample_types[n], xpd=T, pos=1)
bp=barplot(t(rpm["AT3G18780",]), main="actin - after", names.arg="",
        col=colors, xlab="", ylab="reads per million",beside=T)
text(x=bp[n], y=0, labels=sample_types[n], xpd=T, pos=1)
```         

Normalizing by library size reduced variance within groups.

### Examine expression of possible positive controls

To determine if the treatment succeeded in activating ANT and changing expression of ANT targets, we need a positive control.

However, no such positive control is available.

In lieu of a positive control, I'll use the top 6 most differentially expressed genes from:

* [RNA-Seq Links the Transcription Factors AINTEGUMENTA and AINTEGUMENTA-LIKE6 to Cell Wall Remodeling and Plant Defense Pathways](https://www.ncbi.nlm.nih.gov/pubmed/27208279)

In that study, RNA-Seq analysis of floral buds from wildtype Landsberg erecta and from mutant plants lacking functional ANT and AIL6 identified many thousands of DE genes.

AT1G30020, AT1G35490, and AT1G50290 were down-regulated in the mutant. AT1G30835, AT1G35115, and AT1G35320 were up-regulated in the mutant than the wild type.

#### Visualize expression of down-regulated genes

```{r fig.height=4}
par(mfrow=c(1,3))
DrawGeneSampleValues("AT1G30020") 
DrawGeneSampleValues("AT1G35490") 
DrawGeneSampleValues("AT1G50290") 
```

Since expression of the above genes was lower in the ant ail6 double mutant, I expected them to be up-regulated by application of DEX in the plants bearing the GR:ANT construct. 

They have moderate to high expression values in all samples, and expression was only weakly affected by the treatment, if at all. Formal statistical analysis is required to be assess differential expression because the effects are non-existant or too subtle to observe here.

#### Visualize expression of up-regulated genes

```{r fig.height=4}
par(mfrow=c(1,3))
DrawGeneSampleValues("AT1G30835") 
DrawGeneSampleValues("AT1G35115") 
DrawGeneSampleValues("AT1G35320") 
```

Since the above genes were up-regulated in the ant ail6 double mutant, I expected them to be down-regulated by ANT.  They had moderate to low values (RPKM < 0.2) in all samples. As before, they did not seem to respond to the treatment.

None of these genes fit expecations for expression. However, there were many thousands of genes that were differentially expressed in the ant ail6 double mutant. Many or most of these were unlikely to be direct targets of either transcription factor. 

#### Visualize expression of putative ANT target identified by ChIP

```{r fig.height=4,fig.width=3}
DrawGeneSampleValues("AT2G45190")
```

Expression was unaffected by the treatment or the effects were too subtle to observe via visualization.

### Investigate expression changes over time

I expect that genes regulated by the circadian clock were differentially expressed between different times points. 

```{r, echo=FALSE}
g1="AT1G01060"
g1.sym="LHY"
g2="AT2G40080"
g2.sym="ELF4"
g3="AT2G46830"
g3.sym="CCA1"
```

To test this, examine expression of the following clock-regulated genes:

* `r g1.sym` (`r g1`)
* `r g2.sym` (`r g2`)
* `r g3.sym` (`r g3`)

```{r fig.height=4.5,fig.width=6}
par(mfrow=c(1,3))
main=paste(g1,g1.sym)
DrawGeneSampleValues(g1,main=main)
main=paste(g2,g2.sym)
DrawGeneSampleValues(g2,main=main)
main=paste(g3,g3.sym)
DrawGeneSampleValues(g3,main=main) 
par(mfrow=c(1,1))
```

The three genes were expressed differently depending on time of day.

This confirmed time point labeling for samples. 

# Conclusions    

**How many reads aligned in each library, and how many were assigned to genes?**  

Each library has between `r min(mal)` and `r max(mal)` million aligned reads, with an average of `r round(mean(mal),1)`. See Alignment/results/AlignmentSummaryTable.txt for details.  

Each library has between `r min(mass)` and `r max(mass)` million assigned reads, with an average of `r round(mean(mass),1)`. See CountsData/data/ANT-GR_CountsPerGene.summary.txt.

**How are the reads distributed across genes?**  

The top `r top`% of all genes with counts take up `r round(mean(percent),0)`% of all normalized counts.

**Is [positive control gene] higher in the treated than in the non-treated samples?**    

We don't really have a solid positive control gene. None of the genes I selected from the previous study gave the results I would expect for a positive control.  This could be the result of insufficient treatment, or it could be because these genes are not good positive controls.

**Do the samples in the same time collection have similar values for clock genes?**

Yes. It looks like samples were indeed collected at different times of day and it does not look like any samples are in the wrong group (at least in terms of time of collection).

###Session Info

```{r}
sessionInfo()
getRawCounts
DrawGeneSampleValues
```