---
title: "Genomic Sequences"
output:
  html_document: 
    toc: true
---

```{r setup, echo=FALSE}
library(knitr)
knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",", digits=2)
})
```

# Introduction

For several other modules, we will want to have the genomic sequences for certain regions.  This is used for finding de novo motifs, searching for known motifs, etc.

# Analysis

```{r}
suppressPackageStartupMessages(library(rtracklayer))
suppressPackageStartupMessages(library(GenomicRanges))
suppressPackageStartupMessages(library(BSgenome))
source("../src/ProjectWide.R")
source("../src/BedFiles.R")
```


## Sequence data
Download the Arabidopsis genome sequence.
```{r}
fname="data/A_thaliana_Jun_2009.2bit"
if (!file.exists(fname)){
	url="http://igbquickload.org/A_thaliana_Jun_2009/A_thaliana_Jun_2009.2bit"
	download.file(url, destfile = fname)
}
```

Connect to the 2bit file from R.
```{r}
a2bit = import(con=fname)
#names(a2bit) = tolower(names(a2bit)) # to match bed file
names(a2bit) = gsub("Chr", "chr", names(a2bit)) # but keep chrM and chrC in caps
```

## Regions of Interest

How big is each region?
```{r}
seqSize = 1000
```

Annotated gene regions.
```{r}
T10 = read.bed("../ExternalDataSets/TAIR10.bed.gz")
# use the gene id rather than the transcript ID to match de gene list
T10$ID = truncateTranscriptID(T10$name)
# only keep one promoter region for each gene (the first one that appears in the file)
T10 = T10[!duplicated(T10$ID),]

problematic = c("ATMG01410") # this gene creates a promoter region that is passes the end of the chrm
T10 = T10[!T10$ID %in% problematic,]
```


Differentially Expressed Genes
```{r}
fdr=getFDR()
fdr=0.1
# the getDEtable expects the FDR value to be named
fdrSet = rep(fdr, 4)
names(fdrSet) = c("at2hours", "at4hours", "at8hours", "GLM")
# get DE genes
de2 = getDEtable(comparison = "at2hours", maxFDR = fdrSet["at2hours"])
de4 = getDEtable(comparison = "at4hours", maxFDR = fdrSet["at4hours"])
de8 = getDEtable(comparison = "at8hours", maxFDR = fdrSet["at8hours"])
deGLM = getDEtable(comparison = "GLM", maxFDR = fdrSet["GLM"])
#
neverDE = setdiff(T10$ID, c(row.names(de2),
														row.names(de4),
														row.names(de8),
														row.names(deGLM)))
```


```{r}
# bedFiles = c("../ExternalDatasets/GeneStructures/promoter-1kb.bed",
# 						 "../ExternalDatasets/GeneStructures/downstream-1kb.bed")
# names(bedFiles) = c("promoter1kb", "downstream1kb")
# beds = lapply(bedFiles, read.bed)

beds = list(allPromoters = getPromoterRange(T10, size=seqSize),
						allDownstreams = getDownstreamRange(T10, size=seqSize),
						DEat2Promoters = getPromoterRange(T10, size=seqSize, 
																							genes = row.names(de2)),
						DEUPat2Promoters = getPromoterRange(T10, size=seqSize, 
																								genes = row.names(de2)[de2$logFC.at2hours > 0]),
						DEat4Promoters = getPromoterRange(T10, size=seqSize, 
																							genes = row.names(de4)),
						DEat8Promoters = getPromoterRange(T10, size=seqSize, 
																							genes = row.names(de8)),
						DEGLMPromoters = getPromoterRange(T10, size=seqSize, 
																							genes = row.names(deGLM)),
						DEUPGLMPromoters = getPromoterRange(T10, size=seqSize, 
																								genes = row.names(deGLM)[deGLM$logFC.GLM > 0]),
						nonDEpromoters = getDownstreamRange(T10, size=seqSize,
																								genes = neverDE))

# the getPromoterRange function returns names promStart, promEnd, 
# we need chromStart and chromEnd
beds = lapply(beds, function(b){
	names(b)[names(b) == "promStart"] = "chromStart"
	names(b)[names(b) == "promEnd"] = "chromEnd"
	return(b)
	})

beds = lapply(beds, function(b){
	newvals = b$chromStart + 1
	b$chromStart = newvals
	return(b)
})
```


## Retreive sequences

Make each list a GRanges object.
```{r}
topGRanges = lapply(beds, function(pks){
	peakRanges = IRanges(start = pks$chromStart,
											 end = pks$chromEnd,
											 names = pks$chromosome)
	GRan = GRanges(seqnames = pks$chromosome,
					ranges = peakRanges,
					mcols=data.frame(ID=pks$ID))
	return(GRan)
})
```

Get the sequences for each list. This is a DNAStringSet
```{r}
topSeq = lapply(topGRanges, function(GRan){
	seq = getSeq(a2bit, GRan)
	peakNames = as.character(GRan$mcols.ID)
	names(seq) = peakNames
	return(seq)
})
```

## Save sequences

Save sequences in fasta format.  
```{r}
for (setName in names(beds)){
	num = length(topSeq[[setName]])
	fileName = paste0("intermediateFiles/", setName, "_", 
										seqSize, "bp_", 
										num, "seqs.fasta")
	writeXStringSet(topSeq[[setName]], fileName, format="fasta")
	
	# print the filename -- mainly so I can search for it later and find this
	print(fileName)
}
```


