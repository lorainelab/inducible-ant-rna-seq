#!/bin/sh

#  FastqTotals.sh
#  
#
#  Created by Ivory Balkley on 2/12/15.
#

# Use this script to extract the total number of reads 
# in a fastq based on the fastqc report.
# Recommended usage: $ FastqTotals.sh > FastqTotals.txt


for f in $(ls *fastqc.zip)
    do
        FILE=${f%".zip"}
        TOTAL=$(unzip -c $f $FILE/fastqc_data.txt | grep "Total Sequences" - )
        SAMPLE=${f/%"_fastqc.zip"/"\t"}
        echo ${TOTAL/#"Total Sequences"/$SAMPLE}
    done
