---
title: "FastQCsummary"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---


# Introduction
FastQC is a fast and easy to inspect the quality of sequencing. The scripts we used to run FastQC are in the src folder. Each sample is processed alone to make an individual report. Here we create a single report to look over all samples at once.

Our main objective in running FastQC is to find any samples that should not be included in the analysis or that only be included after some amount of trimming.


```{r}
files <- dir(pattern="*fastqc")
accessions <- gsub(files, pattern="_fastqc", replacement="")
names(files) <- accessions
# runTable <- read.delim("../sra/SraRunTable.txt", stringsAsFactors=F)
# samples <- runTable$Sample_Name_s
# names(samples) = accessions
samples = accessions
names(samples) = accessions
```


# Basic Summary
```{r}
Summary = data.frame(matrix(data=NA, nrow=11, ncol=length(files)))
names(Summary) = accessions
for (accession in accessions) {
  fname=paste0(files[accession], "/summary.txt")
  summary = read.delim(fname, header=F)
  Summary[,accession]=summary$V1
  }
row.names(Summary) = as.character(summary$V2)
Summary
```


# Per base sequence quality
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/per_base_quality.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`

# Per sequence quality scores
```{r}
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/per_sequence_quality.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
  }
```
`r DisplayImages`

# Per base sequence content
```{r}
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/per_base_sequence_content.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
  }
```
`r DisplayImages`

# Per base GC content
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/per_base_gc_content.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`

# Per sequence GC content
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/per_sequence_gc_content.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`

# Per base N content
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/per_base_n_content.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`

# Sequence Length Distribution
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/sequence_length_distribution.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`

# Sequence Duplication Levels
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/duplication_levels.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`

# Overrepresented sequences
This module does not have an image.
```{r}
for (accession in accessions) {
startLine=as.numeric(system(intern=T, 
                            paste0('grep -n Overrepresented ', files[accession], '/fastqc_data.txt | cut -f1 -d :')))
stopLine=as.numeric(system(intern=T, 
                           paste0('grep -n Kmer ', files[accession], '/fastqc_data.txt | cut -f1 -d :')))
stopLine=stopLine-1
if ((stopLine-startLine)>1){
  print(samples[accession])
  t=read.delim(file=pipe(paste0("sed -n '", startLine+1, ",", stopLine-1, "p' ", files[accession], "/fastqc_data.txt")))
  print(t)
}
}
```


# Kmer Content
```{r}
#<img src="`r fname.fig6e`">
DisplayImages=""
for (accession in accessions) {
  img=paste0(samples[accession], ':<img src="', files[accession], '/Images/kmer_profiles.png" style="width:2in">')
  DisplayImages = paste(DisplayImages, img, sep="\n")
}
```
`r DisplayImages`


**Housekeeping**
```{r housekeeping}
sessionInfo()
```