# Use this function to draw a diagram of 4 squares with proportional area.
# See SquaredVennMethodAssistant.png for a helpful diagram in understandin/troubleshooting this function
# fname - name of the png file to be exported (optional).
# labels - named vector of values for non-overlaping regions (N,S,E,W) 
#      and overlaping regions (NE,SE,SW,NW), 
#      corresponding to cardinal directions in the final plot
# col - vector of color values for each rectangle in the order N,S,E,W
# names - labels to use for each rectangle in the order N,S,E,W
# inflate - numeric value used to multiply the size of all areas to make the are of the plot larger relative to the text.
# thickness - lwd for arrows and box lines.
# tcex - cex for drawing category labels
# arrDist - distance between arrow and box. 
# drawArrow - T/F if set to FALSE, no arrows are drawn.
# a, d, h, gap - These can be used to change the overal shape of the diagram.  See SquaredVennMethodAssistant.png
# callOut - T/F should the plot include callouts for numbers labeling spaces that are too small for text
draw4SquareVenn <- function(fname=NULL, 
                            labels, 
                            col=c(1,1,1,1), 
                            names=rep("", 4), 
                            inflate=1, 
                            thickness=6, 
                            tcex=3, 
                            arrDist=4, drawArrow=T,
                            a=NULL, d=NULL, h=NULL, gap=NULL, callOut=TRUE){
  
  saveImage=!is.null(fname)
  #labels=c(N,S,E,W,NE,SE,SW,NW)
  #names(labels)=c("N","S","E","W","NE","SE","SW","NW")
  attach(as.list(labels))
  print(labels)
  
  # inflate the plot by multiplying all values
  vals = labels*inflate
  for (i in 1:8){
    assign(names(vals)[i], vals[i])
  }
  
  #totals to use in math
  Nall=N+NE+NW
  Sall=S+SW+SE
  Eall=E+NE+SE
  Wall=W+SW+NW
  
  # This is a suggested way to set a, d, and h. It may have to be set manually to make things look balanced.
  littleSquare=min(NE,NW,SE,SW)
  #   ls=sqrt(littleSquare)
  #   ls=sqrt(137)
  bigSquare=max(Nall,Sall,Eall,Wall)
  bs=sqrt(bigSquare)
  
  if (is.null(a)){a=bs}
  if (is.null(d)){d=a*.3}
  if (is.null(h)){h=d}
  if (is.null(gap)){gap=(a-d-h)*.3}
  # figure up the lengths
  
  b=Sall/a
  c=Nall/a
  
  f=NW/d
  e=SW/d
  g=Wall/(e+f+gap)
  
  i=NE/h
  j=SE/h
  k=Eall/(i+j+gap)
  
  # for plotting, the origin is placed at the lower left corner of the S rectangle.
  
  # figure out the center for each section
  # in the order N-S-E-W-NE-SE-SW-NW
  x=c(a/2, a/2,(a-h+(k/2)), (d-(g/2)), a-(h/2), a-(h/2), d/2, d/2)
  y=c((b+gap+(c/2)), b/2, (b+gap+i-((i+j+gap)/2)), (b+gap+f-((e+f+gap)/2)), b+gap+(i/2), b-(j/2), b-(e/2), b+gap+(f/2))
  names(x)=c("N","S","E","W","NE","SE","SW","NW")
  names(y)=c("N","S","E","W","NE","SE","SW","NW")
  
  # the symbols plotting tool askes for the positions of the centers (x and y)
  # and for the widths and heights of each rectangle to draw.
  widths=c(a, a, k, g)
  heights=c(c, b, (i+j+gap), (e+f+gap))
  recMatrix=cbind(widths, heights)
  
  # figure up the plotting space
  xmin=(g-d)*(1.1)
  xmax=(a+k-h)*1.1
  #ymin=(b+gap+c)*.1
  ymin=0
  #ymax=(b+gap+c)*1.1
  ymax=b+gap+c
  
  margin=.5
  
  # figure up the size for the exported png
  figH=ymax+ymin +2*margin
  figW=xmax+xmin +2*margin
  #if the figure itself is 6 inches tall, how wide should it be?
  fileW = 6*(figW/figH)
  #allow a half inch of margin on all sides (total height of 7 inches)
  # fileW = fileW+1 #mai=c(0.5,0.5,0.5,0.5)
  
  # Export Image
  if (saveImage){
  png(filename = fname, 
      width = fileW*inflate, height = 6*inflate, 
      units = "in", res=600, pointsize=9)
  }
  par(oma=c(0,0,0,0), mai=rep(margin,4), xaxt="n", yaxt="n", bg="white", xpd=T)
  
  # Draw rectangles
  symbols(xlim=c(-xmin, xmax), ylim=c(-ymin, ymax), xlab="", ylab="", asp=1, 
          x=x[1:4], y=y[1:4], rectangles=recMatrix, 
          inches=F, fg=col, lwd=thickness, bty="n")
  
  #adjust the x center values for E and W to be in the middle of E and W not Eall and Wall.
  xlabels=x
  xlabels[3:4] = c(a+((k-h)/2), -((g-d)/2))
  # add the values
  #add commas
  commaLabels = labels
  commaLabels[labels<100]=NA #in small areas, the labels will have to be added later as call outs.
  commaLabels = format(commaLabels, big.mark=",", scientific=F)
  commaLabels = gsub(" ", "", commaLabels) #the format function intruduces white space.
  commaLabels = gsub("NA", "", commaLabels)
  tcexs=rep(tcex, length(labels))
  names(tcexs)=names(x)
  tcexs["NW"]=.7*tcex
  text(x=xlabels, y=y, labels=commaLabels, cex=tcexs)
  
  # add callouts for the small boxes
  if (callOut){
    if (commaLabels["SE"]==""){
      x1=x["SE"]
      y1=y["SE"]
      x2=x["SE"]+h  #a+h
      y2=y["SE"]-h  #b-(2*j)
      lines(x=c(x1,x2), y=c(y1,y2), lwd=thickness/2)
      text(x=x2, y=y2, labels=labels["SE"], adj=c(-.2,.6),cex=tcex)
    }
    if (commaLabels["NW"]==""){
      x1=x["NW"]
      y1=y["NW"]
      x2=x["NW"]-d
      y2=y["NW"]+d
      lines(x=c(x1,x2), y=c(y1,y2), lwd=thickness/2)
      text(x=x2, y=y2, labels=labels["NW"], adj=c(1,0),cex=tcex)
    }
  }
  
  # Add arrows and labels
  arrDist = arrDist #distance between arrow and box
  #arrLength = d
  # North arrow (upward)
  arrLine = 0-arrDist
  y1=(b+gap+c) #+2*arrDist
  #y0=y1-arrLength 
  y0=(b+gap+(c/2))
  if (drawArrow) {
    arrows(x0=arrLine, x1=arrLine, y0=y0, y1=y1, col=col[1], 
           lwd=thickness, code=2, angle=20)
  }
  text(labels=names[1], x=arrLine-arrDist, y=(b+gap+c)+arrDist, 
       adj=c(0,.3), cex=tcex, col=col[1])
  # South arrow (downward)
  arrLine = a+arrDist
  y1=0
  #y0=y1+arrLength 
  y0=b/2
  if (drawArrow) {
    arrows(x0=arrLine, x1=arrLine, y0=y0, y1=y1, col=col[2], 
           lwd=thickness, code=2, angle=20)
  }
  text(labels=names[2], x=arrLine+arrDist, y=0-arrDist*1.3, 
       pos=2, cex=tcex, col=col[2])
  # East arrow (upward)
  arrLine = (a-h+k+arrDist)
  y1=(b+gap+i)
  #y0=y1-arrLength 
  y0=(b-j+((i+gap+j)/2))
  if (drawArrow) {
    arrows(x0=arrLine, x1=arrLine, y0=y0, y1=y1, col=col[3], 
           lwd=thickness, code=2, angle=20)
  }
  text(labels=names[3], x=arrLine+arrDist, y=(b+gap+i)+arrDist, 
       pos=2, cex=tcex, col=col[3])
  # West arrow (downward)
  arrLine = d-g-arrDist
  y1=(b-e)
  #y0=y1+arrLength
  y0=(b-e+((e+gap+f)/2))
  if (drawArrow) {
    arrows(x0=arrLine, x1=arrLine, y0=y0, y1=y1, col=col[4], 
           lwd=thickness, code=2, angle=20)
  }
  text(labels=names[4], x=arrLine-arrDist, y=(b-e)-arrDist*1.3, 
       pos=4, cex=tcex, col=col[4])
  
  if (saveImage){dev.off()}
  
}